
import 'package:flutter/material.dart';

import 'home_screen.dart';

class LoginScreen extends StatefulWidget {

  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State {

  final _formKey = GlobalKey<FormState>();

  final int _minCharLogin = 3; /// Минимальная длина логина
  final int _maxCharLogin = 8; /// Максимальная длина логина

  final int _minCharPass = 8; /// Минимальная длина пароля
  final int _maxCharPass = 16; /// Максимальная длина пароля

  String _login = "";
  String _pass = "";

  /// Проверяем что указанный логин и пароль
  /// имеет право заходить на следующую страницу
  ///
  bool _accountCorrect(String login, String password) {
    return login == 'qwerty' && password == '123456ab';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Авторизация"),
      ),
      body: Container(
          padding: const EdgeInsets.all(10.0),
          child: Form(
              key: _formKey,
              child: Column(children: <Widget>[

                const Spacer(flex: 1),
                const Text(
                    'Введите логин и пароль:',
                    style: TextStyle(fontSize: 20.0)),

                TextFormField(
                  maxLength: _maxCharLogin,
                  onChanged: (value) {
                    setState(() {
                      _login = value;
                    });
                  },
                  decoration: InputDecoration(
                      hintText: "Логин",
                      counterText: "${_login.length} из $_maxCharLogin"
                  ),
                  validator: (String? value) {
                    return value == null || value.isEmpty ?
                    'Введите свое имя' :
                    value.length < _minCharLogin ?
                    'Логин должен содержать не менее $_minCharLogin символов' :
                    null;
                  },
                ),

                TextFormField(
                  maxLength: _maxCharPass,
                  obscureText: true,
                  onChanged: (value) {
                    setState(() {
                      _pass = value;
                    });
                  },
                  decoration: InputDecoration(
                      hintText: "Пароль",
                      counterText: "${_pass.length} из $_maxCharPass"
                  ),
                  validator: (String? value) {
                    return value == null || value.isEmpty ?
                    'Введите пароль' :
                    value.length < _minCharPass ?
                    'Пароль должен содержать не менее $_minCharPass символов' :
                    null;
                  },
                ),

                const Spacer(flex: 2),

                SizedBox(
                    width: double.infinity,
                    height: 40,
                    child:
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          FocusScope.of(context).unfocus();
                          if (_accountCorrect(_login, _pass)) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const HomeScreen(),
                              ),
                            );
                          }
                          else {
                            _showPassIncorrect(context);
                          }
                        }
                      },
                      child:
                      const Text('Вход'),)),
              ],
              )
          )

      ),
    );
  }
  void _showPassIncorrect(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Попробуйте снова"),
          actions: <Widget>[
            ElevatedButton(
              child: const Text("Закрыть"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}